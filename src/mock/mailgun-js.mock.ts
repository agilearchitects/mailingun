export const mailgunJSMock = () => ({
  messages: () => ({
    send: (_: any, callback: any) => callback(false, { id: "" }),
  }),
});
