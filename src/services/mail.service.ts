import { MailingunService } from "./mailingun.service";

export interface IHeaders {
  [key: string]: string;
}

export interface IMailService {
  send(from: string, to: string | string[], subject: string, html: string): Promise<void>;
}

export class MailService implements IMailService {
  public constructor(
    private readonly mailingunService: MailingunService,
  ) { }

  public async send(from: string, to: string | string[], subject: string, html: string, headers?: IHeaders): Promise<void> {
    try {
      await this.mailingunService.send(from, to, subject, html, headers);
      return;
    } catch (error) {
      throw (error);
    }
  }
}
