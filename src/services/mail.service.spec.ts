// Libs
import { expect } from "chai";
import { describe, it } from "mocha";

// Servicees
import { MailService } from "./mail.service";
import { MailingunService } from "./mailingun.service";

// Mock mailgun
import { mailgunJSMock } from "../mock/mailgun-js.mock";

describe("MailService", () => {
  it("Should be able to send email", (done: Mocha.Done) => {
    const mailService = new MailService(new MailingunService(
      "key",
      "domain",
      "api.mailgun.net",
      (mailgunJSMock as any) // Using the mock MailgunJS to avoid sending real emails
    ));
    mailService.send("mail@email.com", "mail@email.com", "Subject", "Message").then(_ => {
      done();
    });
  });
});