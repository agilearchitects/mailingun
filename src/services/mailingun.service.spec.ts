// Lib
import { expect } from "chai";
import { describe, it } from "mocha";
import mailgunJS from "mailgun-js";

// MailingunService service
import { MailingunService } from "./mailingun.service";

// Mock mailgun
import { mailgunJSMock } from "../mock/mailgun-js.mock";

describe("MailingunService", () => {
  it("Should be able to send email", (done: Mocha.Done) => {
    const mailingunService = new MailingunService(
      "key",
      "domain",
      "api.mailgun.net",
      (mailgunJSMock as any) // Using the mock MailgunJS to avoid sending real emails
    );
    mailingunService.send("mail@email.com", "mail@email.com", "Subject", "Message").then((response: mailgunJS.messages.SendResponse) => {
      expect(response).has.property("id");
      done();
    });
  });
  it("Should be able to attach headers to email", async () => {
    const headerName = "X-Custom-Header";
    const headerValue = "custom value";
    const mailingunService = new MailingunService(
      "key",
      "domain",
      "api.mailgun.net",
      (mailgunJSMock as any) // Using the mock MailgunJS to avoid sending real emails
    );
    mailingunService.send("mail@email.com", "mail@email.com", "Subject", "Message", { [headerName]: headerValue });
  });
});