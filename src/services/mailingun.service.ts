import mailgunJS from "mailgun-js";
import { IHeaders } from "./mail.service";

export interface IMailingunService {
  send(from: string, to: string | string[], subject: string, html: string): Promise<mailgunJS.messages.SendResponse>;
}

export class MailingunService implements IMailingunService {
  private mailgunInstance: mailgunJS.Mailgun;

  public constructor(
    private readonly apiKey: string,
    private readonly domain: string,
    private readonly mailgunHost: string,
    private readonly mailgunJSModule: typeof mailgunJS = mailgunJS,
  ) {
    this.mailgunInstance = this.mailgunJSModule({
      apiKey: this.apiKey,
      domain: this.domain,
      host: this.mailgunHost,
    });
  }

  public send(from: string, to: string | string[], subject: string, html: string, headers?: IHeaders): Promise<mailgunJS.messages.SendResponse> {
    return new Promise((resolve, reject) => {
      this.mailgunInstance.messages().send({
        from,
        to,
        subject,
        html,
        ...(headers !== undefined ? Object.keys(headers).reduce(
          (previousValue: IHeaders, currentValue: string) => (
            // Prepend header key value to header
            { ...previousValue, [`h:${currentValue}`]: headers[currentValue] }), {} as IHeaders,
        ) : {}),
      }, (error: any, body: mailgunJS.messages.SendResponse) => {
        if (error) { reject(error); return; }
        resolve(body);
      });
    });
  }
}
